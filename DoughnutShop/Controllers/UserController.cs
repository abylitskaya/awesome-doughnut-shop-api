﻿using DoughnutShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DoughnutShop.Controllers
{
    [EnableCors(origins: "http://localhost:9000", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        private DoughnutShopDbContext db = new DoughnutShopDbContext();

        [HttpGet]
        [Route("users")]
        public IHttpActionResult GetAll()
        {

            List<User> users = db.Users.ToList();

            return Ok(users);
        }

        [HttpGet]
        [Route("users/{id}")]
        public IHttpActionResult Get(int id)
        {

            User user = db.Users.Find(id);

            if (user == null)
            {
                return NotFound();
            }

            user.Password = null;

            return Ok(user);
        }

        [HttpPost]
        [Route("users")]
        public IHttpActionResult Create([FromBody]User user)
        {
            UserCredentials us = new UserCredentials(user);
            user.Password = us.Password;
            db.Users.Add(user);
            db.SaveChanges();

            return Ok(user);
        }

        [HttpPut]
        [Route("users")]
        public IHttpActionResult Update([FromBody]User newUser)
        {
            User user = db.Users.Find(newUser.Id);
            user.Update(newUser);
            db.SaveChanges();

            return Ok(user);
        }

        [HttpDelete]
        [Route("users/{id}")]
        public IHttpActionResult Delete(int id)
        {
            User user = db.Users.Find(id);

            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [Route("users/checkauth")]
        public IHttpActionResult Authenticate([FromBody]UserCredentials user)
        {
            try
            {
                User curUser = db.Users.First((u) => u.Email == user.Email);

                if (user.Compare(curUser))
                {
                    return Ok(curUser);
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception e)
            {
                return Unauthorized();
            }
        

            return Ok();
        }

        [HttpGet]
        [Route("users/{id}/orders")]
        public IHttpActionResult GetUserOrders(int id)
        {
            List<Order> orders = db.Orders.Where(order => order.UserId == id).ToList();

            return Ok(orders);
        }
    }
}
