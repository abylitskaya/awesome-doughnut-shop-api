﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoughnutShop.Models
{
    public class DoughnutShopDbContext : DbContext
    {
        public DbSet<Doughnut> Doughnuts { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<DoughnutOrder> DoughnutOrders { get; set; }
    }
}

/*
 * PM> Enable-Migrations -ContextTypeName DoughnutShop.Models.DoughnutShopDbContext
 * PM> Add-Migration DoughnutShopMigration
 */