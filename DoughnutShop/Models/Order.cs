﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoughnutShop.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }

        public int UserId { get; set; }

        public string Address { get; set; }

        public virtual List<DoughnutOrder> DoughnutOrders { get; set; }

        public DateTime Date { get; set; }
        
        public double TotalPrice { get; set; }

        public void Update(Order o)
        {
            Date = DateTime.Now;
            Address = o.Address;
            //TotalPrice = 
        }
    }
}
