﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DoughnutShop.Models
{
    public class DoughnutOrder
    {
        [Key]
        public int Id { get; set; }

        public int OrderId { get; set; }

        public int DoughnutId { get; set; }

        [ForeignKey("DoughnutId")]
        public virtual Doughnut Doughnut { get; set; }

        public int Count { get; set; }
    }
}