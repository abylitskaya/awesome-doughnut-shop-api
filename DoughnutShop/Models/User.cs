﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoughnutShop.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Role { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Address { get; set; }

        public User() { }

        public User(User u)
        {
            Name = u.Name;
            Role = u.Role;
            Email = u.Email;
            UserCredentials credantials = new UserCredentials(u);
            Password = credantials.Password;
        }

        public void Update(User u)
        {
            Name = u.Name;
            Role = u.Role;
            Email = u.Email;
            Address = u.Address;
        }
    }
}